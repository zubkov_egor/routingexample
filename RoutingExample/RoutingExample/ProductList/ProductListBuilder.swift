//
//  ProductListBuilder.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

final class ProductListBuilder {
    
    let router: ProductListRouter
    let categoryId: String
    
    // MARK: - Initialization
    init(with router: ProductListRouter, categoryId: String) {
        self.router = router
        self.categoryId = categoryId
    }
    
    func build() -> UIViewController {
        let productListVC = ProductListVC(with: router, categoryId: categoryId)
        // setup other dependencies
        return productListVC
    }
    
}
