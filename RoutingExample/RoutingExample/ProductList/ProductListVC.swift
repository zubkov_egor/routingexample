//
//  ProductListVC.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

final class ProductListVC: UIViewController {

    let router: ProductListRouting
    let categoryId: String
    
    // MARK: - Initialization
    init(with router: ProductListRouter, categoryId: String) {
        self.router = router
        self.categoryId = categoryId
        super.init(nibName: "ProductListVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Product List"
    }

    @IBAction private func pdpAction() {
        router.showProduct(with: "123")
    }
}
