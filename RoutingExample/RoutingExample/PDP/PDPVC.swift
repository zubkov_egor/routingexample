//
//  PDPVC.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

final class PDPVC: UIViewController {

    let router: PDPRouting
    let productId: String
    
    // MARK: - Initialization
    init(with router: PDPRouting, productId: String) {
        self.router = router
        self.productId = productId
        super.init(nibName: "PDPVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "PDP"
        
        if presentingViewController != nil {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelButtonAction))
        }
    }
    
    @objc func cancelButtonAction() {
        dismiss(animated: true, completion: nil)
    }
}
