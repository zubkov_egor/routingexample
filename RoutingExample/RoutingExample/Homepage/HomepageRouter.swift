//
//  HomepageRouter.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import Foundation

protocol HomepageRoutingOutputHandling {
    func handleCategorySelection(by categoryId: String)
    func handleRecommendedProductSelection(by productId: String)
}

protocol HomepageRouting {
    func showProducts(with categoryId: String)
    func showRecommendedProduct(with productId: String)
}

final class HomepageRouter {
    
    let routingHandler: HomepageRoutingOutputHandling
    
    // MARK: - Initialization
    init(routingHandler: HomepageRoutingOutputHandling) {
        self.routingHandler = routingHandler
    }
}

extension HomepageRouter: HomepageRouting {
    func showProducts(with categoryId: String) {
        routingHandler.handleCategorySelection(by: categoryId)
    }
    
    func showRecommendedProduct(with productId: String) {
        routingHandler.handleRecommendedProductSelection(by: productId)
    }
}
