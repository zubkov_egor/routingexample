//
//  ProductListRouter.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

protocol ProductListRoutingOutputHandling {
    func handleProductSelection(by productId: String)
}

protocol ProductListRouting {
    func showProduct(with productId: String)
}

final class ProductListRouter {
    
    let routingHandler: ProductListRoutingOutputHandling
    
    // MARK: - Initialization
    init(routingHandler: ProductListRoutingOutputHandling) {
        self.routingHandler = routingHandler
    }
    
    // MARK: - Public
    func pushModule(in navigationController: UINavigationController, with builder: ProductListBuilder, completion: (() -> Void)?) {
        let vc = builder.build()
        navigationController.pushViewController(vc, animated: true, completion: completion)
    }
}

extension ProductListRouter: ProductListRouting {
    func showProduct(with productId: String) {
        routingHandler.handleProductSelection(by: productId)
    }
}
