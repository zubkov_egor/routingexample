//
//  HomepageVC.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

final class HomepageVC: UIViewController {
    
    let router: HomepageRouting
    
    // MARK: - Initialization
    init(with router: HomepageRouting) {
        self.router = router
        super.init(nibName: "HomepageVC", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Homepage"
    }
    
    // MARK: - Private
    @IBAction private func productListAction() {
        router.showProducts(with: "123")
    }
    
    @IBAction private func pdpAction() {
        router.showRecommendedProduct(with: "123")
    }
}
