//
//  AppState.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import Foundation

enum RoutingType {
    case initial
    case homepage
    case productList
    case pdp
}

struct AppState {
    var currentRoutingType: RoutingType
}
