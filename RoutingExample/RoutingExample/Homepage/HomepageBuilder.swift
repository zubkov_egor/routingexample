//
//  HomepageBuilder.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

final class HomepageBuilder {
    
    let router: HomepageRouter
    
    // MARK: - Initialization
    init(with router: HomepageRouter) {
        self.router = router
    }
    
    func build() -> UIViewController {
        let homepageVC = HomepageVC(with: router)
        // setup other dependencies
        return homepageVC
    }
    
}
