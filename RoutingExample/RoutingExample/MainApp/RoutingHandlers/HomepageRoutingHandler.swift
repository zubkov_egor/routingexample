//
//  HomepageRoutingHandler.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import Foundation

final class HomepageRoutingHandler {
    
    let routingType = RoutingType.homepage
    let sourceRoutingType: RoutingType
    let routingCoordinator: RoutingCoordinating
    
    // MARK: - Initialization
    init(sourceRoutingType: RoutingType, routingCoordinator: RoutingCoordinating) {
        self.sourceRoutingType = sourceRoutingType
        self.routingCoordinator = routingCoordinator
    }
}

extension HomepageRoutingHandler: RoutingHandling {
    func present(completion: (() -> Void)?) {
        
    }
}

extension HomepageRoutingHandler: HomepageRoutingOutputHandling {
    func handleCategorySelection(by categoryId: String) {
        let productListRoutingHandler = ProductListRoutingHandler(sourceRoutingType: routingType, routingCoordinator: routingCoordinator, categoryId: categoryId)
        routingCoordinator.enqueue(handler: productListRoutingHandler)
    }
    
    func handleRecommendedProductSelection(by productId: String) {
        let pdpRoutingHandler = PDPRoutingHandler(sourceRoutingType: routingType, routingCoordinator: routingCoordinator, productId: productId)
        routingCoordinator.enqueue(handler: pdpRoutingHandler)
    }
}
