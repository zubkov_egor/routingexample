//
//  PDPRoutingHandler.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import Foundation

final class PDPRoutingHandler {
    
    let routingType = RoutingType.pdp
    let sourceRoutingType: RoutingType
    let routingCoordinator: RoutingCoordinating
    let productId: String
    
    // MARK: - Initialization
    init(sourceRoutingType: RoutingType, routingCoordinator: RoutingCoordinating, productId: String) {
        self.sourceRoutingType = sourceRoutingType
        self.routingCoordinator = routingCoordinator
        self.productId = productId
    }
}

extension PDPRoutingHandler: RoutingHandling {
    func present(completion: (() -> Void)?) {
        let pdpRouter = PDPRouter(routingHandler: self)
        let pdpBuilder = PDPBuilder(with: pdpRouter, productId: productId)
        
        switch sourceRoutingType {
        case .homepage:
            pdpRouter.presentModule(in: routingCoordinator.mainNavigationController, with: pdpBuilder, completion: completion)
        default:
            pdpRouter.pushModule(in: routingCoordinator.mainNavigationController, with: pdpBuilder, completion: completion)
        }
    }
}

extension PDPRoutingHandler: PDPRoutingOutputHandling { }
