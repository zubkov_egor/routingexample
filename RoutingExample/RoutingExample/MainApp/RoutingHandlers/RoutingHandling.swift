//
//  RoutingHandling.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import Foundation

protocol RoutingHandling {
    var routingType: RoutingType { get }
    var sourceRoutingType: RoutingType { get }
    var routingCoordinator: RoutingCoordinating { get }

    func present(completion: (() -> Void)?)
}
