//
//  PDPRouter.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

protocol PDPRoutingOutputHandling { }

protocol PDPRouting { }

final class PDPRouter {
    
    let routingHandler: PDPRoutingOutputHandling
    
    // MARK: - Initialization
    init(routingHandler: PDPRoutingOutputHandling) {
        self.routingHandler = routingHandler
    }
    
    // MARK: - Public
    func pushModule(in navigationController: UINavigationController, with builder: PDPBuilder, completion: (() -> Void)?) {
        let vc = builder.build()
        navigationController.pushViewController(vc, animated: true, completion: completion)
    }
    
    func presentModule(in navigationController: UINavigationController, with builder: PDPBuilder, completion: (() -> Void)?) {
        let vc = builder.build()
        navigationController.present(UINavigationController(rootViewController: vc), animated: true, completion: completion)
    }
}

extension PDPRouter: PDPRouting { }
