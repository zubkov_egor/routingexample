//
//  PDPBuilder.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

final class PDPBuilder {
    
    let router: PDPRouter
    let productId: String
    
    // MARK: - Initialization
    init(with router: PDPRouter, productId: String) {
        self.router = router
        self.productId = productId
    }
    
    func build() -> UIViewController {
        let pdpVC = PDPVC(with: router, productId: productId)
        // setup other dependencies
        return pdpVC
    }
    
}
