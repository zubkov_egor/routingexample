//
//  UINavigationController+Completion.swift
//  Shop
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

@objc
extension UINavigationController {
    
    @objc func pushViewController(_ viewController: UIViewController, animated: Bool, completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    
}
