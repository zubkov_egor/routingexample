//
//  AppDelegate.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var routingCoordinator: RoutingCoordinator?
    
    // MARK: - UIApplicationDelegate
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        presentHomepage()
        
        return true
    }
    
    // MARK: - Private
    private func presentHomepage() {
        let navigationController = UINavigationController()
        let appState = AppState(currentRoutingType: .homepage)
        let routingCoordinator = RoutingCoordinator(appState: appState, mainNavigationController: navigationController)
        self.routingCoordinator = routingCoordinator
        
        let homepageRoutingHandler = HomepageRoutingHandler( sourceRoutingType: .initial, routingCoordinator: routingCoordinator)
        let homepageRouter = HomepageRouter(routingHandler: homepageRoutingHandler)
        let homepageBuilder = HomepageBuilder(with: homepageRouter)
        
        navigationController.viewControllers = [homepageBuilder.build()]
        window?.rootViewController = navigationController
    }
}

