//
//  ProductListRoutingHandler.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import Foundation

final class ProductListRoutingHandler {
    
    let routingType = RoutingType.productList
    let sourceRoutingType: RoutingType
    let routingCoordinator: RoutingCoordinating
    let categoryId: String
    
    // MARK: - Initialization
    init(sourceRoutingType: RoutingType, routingCoordinator: RoutingCoordinating, categoryId: String) {
        self.sourceRoutingType = sourceRoutingType
        self.routingCoordinator = routingCoordinator
        self.categoryId = categoryId
    }
}

extension ProductListRoutingHandler: RoutingHandling {
    func present(completion: (() -> Void)?) {
        let productListRouter = ProductListRouter(routingHandler: self)
        let productListBuilder = ProductListBuilder(with: productListRouter, categoryId: categoryId)
        productListRouter.pushModule(in: routingCoordinator.mainNavigationController, with: productListBuilder, completion: completion)
    }
}

extension ProductListRoutingHandler: ProductListRoutingOutputHandling {
    func handleProductSelection(by productId: String) {
        let pdpRoutingHandler = PDPRoutingHandler(sourceRoutingType: routingType, routingCoordinator: routingCoordinator, productId: productId)
        routingCoordinator.enqueue(handler: pdpRoutingHandler)
    }
}
