//
//  RoutingCoordinator.swift
//  RoutingExample
//
//  Created by Egor Zubkov on 18/10/2018.
//  Copyright © 2018 Egor Zubkov. All rights reserved.
//

import UIKit

protocol RoutingCoordinating {
    var mainNavigationController: UINavigationController { get }
    
    func enqueue(handler: RoutingHandling)
}

final class RoutingCoordinator {
    var appState: AppState
    let mainNavigationController: UINavigationController
    
    private(set) var routingHandlersQueue: [RoutingHandling] = []
    
    init(appState: AppState, mainNavigationController: UINavigationController) {
        self.appState = appState
        self.mainNavigationController = mainNavigationController
    }
}

extension RoutingCoordinator: RoutingCoordinating {
    func enqueue(handler: RoutingHandling) {
        guard routingHandlersQueue.isEmpty else {
            routingHandlersQueue.append(handler)
            return
        }
        proceed(routingHandling: handler)
    }
    
    private func proceed(routingHandling: RoutingHandling) {
        routingHandling.present { [weak self] in
            guard let strongSelf = self, let handler = strongSelf.routingHandlersQueue.last else {
                return
            }
            // update AppState in accordance with presented module
            strongSelf.appState.currentRoutingType = routingHandling.routingType
            
            // present next screen from the queue
            strongSelf.proceed(routingHandling: handler)
        }
    }
}
